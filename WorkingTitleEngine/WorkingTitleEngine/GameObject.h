#pragma once
#include <memory>
#include <unordered_map>
#include <type_traits> 
#include <typeindex>
#include <string>

typedef unsigned int ObjectID;

class Component;

class GameObject
{
public:

	GameObject() = default;
	GameObject(ObjectID _ID, std::string _name);

	virtual ~GameObject();

	std::string name;

	void Update();

	ObjectID getID();



	/* Template functions */
	template <typename T>
	Component* AddComponent()
	{
		//make object, and add unique ptr to vector,
		// and return its ptrs
		auto newComp = (Components[typeid(T)] = std::make_unique<T>()).get();
		return newComp;
	}

	template <typename T>
	bool RemoveComponent()
	{
		Components.erase(Components[typeid(T)]);
	}

	template <typename T>
	Component* FindComponent()
	{
		try
		{
			return Components[typeid(T)].get();
		}
		catch (std::out_of_range)
		{
			return nullptr;
		}
	}

	template <typename T>
	Component* GetComponent()
	{
		if (Components.count(typeid(T)) != 0)
		{
			return Components[typeid(T)].get();
		}
		else
		{
			return nullptr;
		}
	}

	template <typename T>
	T* GetComponentAs()
	{
		if (Components.count(typeid(T)) != 0)
		{
			auto returnComp = Components[typeid(T)].get();
			return static_cast<T*>(returnComp);
		}
		else
		{
			return nullptr;
		}
	}

private:
	ObjectID ID;
	//Remember: typeid(T) returns a ptr to a static (life-time) "const" std::type_info
	//std::type_index encapsulates it, taking care of the const and the de-referencing for us
	std::unordered_map<std::type_index, std::unique_ptr<Component> > Components;
};

