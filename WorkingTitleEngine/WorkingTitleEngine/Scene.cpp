#include "Scene.h"
#include "GameObject.h"


Scene::Scene()
{
	currentIDIndex = 0;
}

Scene::~Scene()
{}

GameObject* Scene::CreateGameObject()
{
	return CreateGameObject("NewObj");
}

//Had to use a lot of dumb workarounds here
GameObject* Scene::CreateGameObject(std::string _name)
{
	std::string name = _name;

	int incrementation = 1;

	while (Find(name))
	{
		name = _name + '(' + std::to_string(incrementation) + ')';
		++incrementation;
	}

	ObjectID id = currentIDIndex;
	++currentIDIndex;

	//This was giving me an compiler error if i declared it any way but like this
	gameObjects.push_back(std::make_unique<GameObject>(currentIDIndex, name));


	/* Previous attempt */
	//std::unique_ptr<GameObject>* newObj = &std::make_unique<GameObject>(currentIDIndex, name);
	//gameObjects.push_back(*newObj);
	//return newObj->get(); // wanted to use this line instead so I wouldn't have to search vector again

	return Find(name); 
}

bool Scene::DestroyGameObject(GameObject* _toDestroy)
{
	for (auto &iter : gameObjects)
	{
		if (iter.get()->getID() == _toDestroy->getID())
		{
			//objectsToDelete.push_back(iter);
			gameObjects.erase(std::find(gameObjects.begin(), gameObjects.end(), iter));

			return true;
		}
	}

	return false;
}

GameObject* Scene::Find(std::string _name)
{

	for (auto &iter : gameObjects)
	{
		if (iter.get()->name == _name)
		{
			return iter.get();
		}
	}

	return nullptr;
}

//void Scene::DeleteObjectsInBuffer()
//{
//	objectsToDelete.clear();
//}
