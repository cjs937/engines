#pragma once
#include <vector>
#include <string>
#include <memory>

typedef unsigned int ObjectID;
class GameObject;

class Scene
{
private:
	ObjectID currentIDIndex;

public:
	Scene();
	virtual ~Scene();

	std::vector<std::unique_ptr<GameObject>> gameObjects;
	//std::vector<std::unique_ptr<GameObject>> objectsToDelete;

	GameObject* CreateGameObject();
	GameObject* CreateGameObject(std::string _name);

	bool DestroyGameObject(GameObject* _toDestroy);
	GameObject* Find(std::string _name);

	//Call at end of update loop
	//void DeleteObjectsInBuffer();

};

