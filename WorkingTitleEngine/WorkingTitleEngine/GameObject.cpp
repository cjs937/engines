#include "GameObject.h"
#include "Component.h"


GameObject::GameObject(ObjectID _ID, std::string _name) //: ID(_ID), name(_name)
{
	ID = _ID;
	name = _name;
}


GameObject::~GameObject()
{
	Components.clear();
}

void GameObject::Update()
{
	for (auto &pair : Components)    //Note: unique_ptr cannot be copied, so we need a reference
	{
		pair.second->Update();
	}
}

ObjectID GameObject::getID() 
{ 
	return ID; 
}
