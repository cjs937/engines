#include "Scene.h"
#include "GameObject.h"
#include "Component.h"
#include <iostream>
#include <string>

int main()
{
	Scene* testScene = new Scene();

	GameObject* obj1 = testScene->CreateGameObject();
	GameObject* obj2 = testScene->CreateGameObject();

	std::cout << obj1->name << " " << obj2->name << "\n";

	if (!obj1->FindComponent<Component>())
	{
		std::cout << "No component found\n";
	}

	obj2->AddComponent<Component>();
	if (obj2->FindComponent<Component>())
	{
		std::cout << "Found it\n";
	}

	obj2->GetComponent<Component>()->Update();

	std::string objName = obj1->name;

	testScene->DestroyGameObject(obj1);

	//testScene->DeleteObjectsInBuffer();

	if (!testScene->Find(objName))
	{
		std::cout << "Obj1 deleted successfully\n";
	}

	std::cin.get();
	return 0;
}